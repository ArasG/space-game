import java.awt.*;

public class Plane extends Rectangle {

    private  int cordPlaneX1 = 20;
    private  int cordPlaneX2 = 20;
    private  int cordPlaneX3 = 60;
    private  int cordPlaneY1 = 270;
    private  int cordPlaneY2 = 290; //290
    private  int cordPlaneY3 = 280;
    private  int velocityPlaneY;


    public  void movePlane() {
        if (cordPlaneY1 < 0) {
            cordPlaneY1 = 0;
            cordPlaneY2 = 20;
            cordPlaneY3 = 10;
            velocityPlaneY = 0;
        }

        if (cordPlaneY2 > 560) {
            cordPlaneY1 = 540;
            cordPlaneY2 = 560;
            cordPlaneY3 = 550;
            velocityPlaneY = 0;
        }

        cordPlaneY1 = cordPlaneY1 + velocityPlaneY;
        cordPlaneY2 = cordPlaneY2 + velocityPlaneY;
        cordPlaneY3 = cordPlaneY3 + velocityPlaneY;
    }

    public  int getCordPlaneX1() {
        return cordPlaneX1;
    }

    public  int getCordPlaneX2() {
        return cordPlaneX2;
    }

    public  int getCordPlaneX3() {
        return cordPlaneX3;
    }

    public  int getCordPlaneY1() {
        return cordPlaneY1;
    }

    public  int getCordPlaneY2() {
        return cordPlaneY2;
    }

    public  int getCordPlaneY3() {
        return cordPlaneY3;
    }

    public  int getVelocityPlaneY() {
        return velocityPlaneY;
    }

    public void setVelocityPlaneY(int velocityPlaneY) {
        this.velocityPlaneY = velocityPlaneY;
    }
}

