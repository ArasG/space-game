import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class GraphicFlight extends JPanel implements ActionListener, KeyListener {

    Timer timer = new Timer(50, this);
    Plane plane = new Plane();
    Star star = new Star();
    Star star1 = new Star();
    int precisionOfCollision = 20;
    boolean collisionDetected = false;
    int x = 1;


    public GraphicFlight() {
        timer.start();
        addKeyListener(this); // adds keyLisener
        setFocusable(true);
        setFocusTraversalKeysEnabled(false); // disables shift of tab buttons
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.black);
        g.setColor(Color.gray);
        g.fillPolygon(new int[]{plane.getCordPlaneX1(), plane.getCordPlaneX2(), plane.getCordPlaneX3()},
                new int[]{plane.getCordPlaneY1(), plane.getCordPlaneY2(), plane.getCordPlaneY3()}, 3);

        g.setColor(Color.yellow);
        if (collisionDetected) {
            star.setWidthOfStar(1);
            star.setHeightOfStar(1);
            collisionDetected = false;
        }
        g.fillRect(star.getCordStarX1(), star.getCordStarY1(), star.getWidthOfStar(), star.getHeightOfStar());
        g.fillRect(star1.getCordStarX1(), star1.getCordStarY1(), star1.getWidthOfStar(), star1.getHeightOfStar());
    }
//NOT WORKING
    /*public void checkCollision() {
        if (plane.intersects(star)) {
            pointScored = true;
            System.out.println(pointScored);
            System.out.println("scored");
        }
    }*/

    public void checkCollision2() {
        if ((Math.abs((plane.getCordPlaneX3() - precisionOfCollision) - star.getCordStarX1())) <= precisionOfCollision &&
                (Math.abs(plane.getCordPlaneY3() - star.getCordStarY1())) <= precisionOfCollision) {
            collisionDetected = true;
        }
    }

    public void starCollected() {
        if (collisionDetected) {
            star.setCordStarX1(-50);
            collisionDetected = false;
        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        star.moveStar();
        // star1.moveStar();
        plane.movePlane();
        checkCollision2();
        //starCollected();
        repaint();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int c = e.getKeyCode(); //get button code
        System.out.println(c);

        if (c == KeyEvent.VK_UP) {
            plane.setVelocityPlaneY(-10);

        }
        if (c == KeyEvent.VK_DOWN) {
            plane.setVelocityPlaneY(10);

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int c = e.getKeyCode(); //get button code
        System.out.println(c);

        if (c == KeyEvent.VK_UP) {
            plane.setVelocityPlaneY(0);

        }
        if (c == KeyEvent.VK_DOWN) {
            plane.setVelocityPlaneY(0);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
}
