import javax.swing.*;


public class MyFrame extends JFrame {
    //GraphicsDemo graphicsDemo = new GraphicsDemo();
    GraphicFlight graphicFlight = new GraphicFlight();

    public MyFrame() {
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(graphicFlight);
        this.setVisible(true);
        this.setResizable(false);
    }
}
