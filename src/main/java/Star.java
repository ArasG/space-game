import java.awt.*;
import java.util.Random;

public class Star extends Rectangle {

    Random random = new Random();
    private int cordStarX1 = 500;
    private int cordStarY1 = random.nextInt(500);
    private int velocityStar = ((random.nextInt(5))+5);
    private int widthOfStar = 20;
    private int heightOfStar = 20;

    public void generateNewStar() {
        widthOfStar = 20;
        heightOfStar = 20;
        cordStarX1 = 500;
        cordStarY1 = random.nextInt(550);
        velocityStar = ((random.nextInt(5))+5);


    }

    public void moveStar() {
        if (cordStarX1 <= 0) {
            generateNewStar();
        }
        cordStarX1 = cordStarX1 - velocityStar;
        System.out.println("FFF:" + cordStarX1);
    }




    public int getCordStarX1() {
        return cordStarX1;
    }

    public void setCordStarX1(int cordStarX1) {
        this.cordStarX1 = cordStarX1;
    }

    public int getCordStarY1() {
        return cordStarY1;
    }

    public void setCordStarY1(int cordStarY1) {
        this.cordStarY1 = cordStarY1;
    }

    public int getVelocityStar() {
        return velocityStar;
    }

    public void setVelocityStar(int velocityStar) {
        this.velocityStar = velocityStar;
    }

    public int getWidthOfStar() {
        return widthOfStar;
    }

    public void setWidthOfStar(int widthOfStar) {
        this.widthOfStar = widthOfStar;
    }

    public int getHeightOfStar() {
        return heightOfStar;
    }

    public void setHeightOfStar(int heightOfStar) {
        this.heightOfStar = heightOfStar;
    }
}
