import sun.security.provider.Sun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.util.Random;

public class GraphicsDemo extends JPanel implements ActionListener {

    Timer timer = new Timer(10, this);
    JLabel label;
    Random random;
    int cordX = 100;
    int cordY = 1;
    int velocityX = 5;
    int velocityY = 5;
    ImageIcon originalImage = new ImageIcon("src/main/Sun.png");
    ImageIcon scaledImage = new ImageIcon(originalImage.getImage()
            .getScaledInstance(originalImage.getIconWidth() / 3,
                    originalImage.getIconHeight() / 3, Image.SCALE_SMOOTH));

    public GraphicsDemo() {
        label = new JLabel(scaledImage);
       label.setSize(100, 100);
        ///label.setLocation(500, -50);
        this.setLayout(null);
        this.add(label);
        timer.start();

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.CYAN);
        g.setColor(Color.red);
        g.fillOval(cordX, cordY, 100, 100);
    }

    public void move() {
        if (cordX >= 485 || cordX <= 0) {
            velocityX = -velocityX;

        }
        if (cordY >= 465 || cordY <= 0) {
            velocityY = -velocityY;
        }
        cordX = cordX + velocityX;
        cordY = cordY + velocityY;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       move();
        repaint();
       label.setLocation(cordX/2,cordY/2);


    }
}
